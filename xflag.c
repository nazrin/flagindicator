#include <X11/XKBlib.h>

static Display* display;
static XkbDescPtr kbdDescPtr;

int getGroupNum(){
	if(!display)
		display = XOpenDisplay("");
	XkbStateRec xkbState;
	XkbGetState(display, XkbUseCoreKbd, &xkbState);
	return xkbState.group;
}

const char* getLayout(){
	if(!display)
		display = XOpenDisplay("");

	if(!kbdDescPtr){
		kbdDescPtr = XkbAllocKeyboard();
		XkbGetNames(display, XkbSymbolsNameMask, kbdDescPtr);
	}

	Atom symName = kbdDescPtr->names->symbols;
	char* layoutString = XGetAtomName(display, symName);
	return layoutString;
}

