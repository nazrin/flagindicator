flagindicator: flag.vala xflag.c
	valac --pkg gtk+-3.0 --pkg appindicator3-0.1 flag.vala xflag.c -o flagindicator

.PHONY: clean
clean:
	rm -fv -- flagindicator

