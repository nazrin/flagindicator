extern int getGroupNum();
extern string getLayout();

string[] getFlags(){
	string layout = getLayout();
	string[] parts = layout.split(":");
	string[] codes = parts[0].split("+")[1:];
	for(int i = 0; i < codes.length; i++)
		codes[i] = codes[i].split("-")[0];
	return codes;
}

int main(string[] args){
	Gtk.init(ref args);

	bool cont = true;

	var menu = new Gtk.Menu();
	var exitItem = new Gtk.MenuItem.with_label("Exit");
	exitItem.activate.connect(() => {
		cont = false;
	});
	menu.append(exitItem);
	menu.show_all();

	var indicator = new AppIndicator.Indicator("flags", "", AppIndicator.IndicatorCategory.APPLICATION_STATUS);
	if(!(indicator is AppIndicator.Indicator))
		return 1;
	indicator.set_status(AppIndicator.IndicatorStatus.ACTIVE);
	indicator.set_menu(menu);

	indicator.icon_theme_path = "./flags/svg/";

	string[] flags = getFlags();
	int oldGroupNum = -1;
	while(cont){
		Gtk.main_iteration_do(true);
		int newGroupNum = getGroupNum();
		if(oldGroupNum != newGroupNum){
			if(newGroupNum < flags.length)
				indicator.set_icon_full(flags[newGroupNum], flags[newGroupNum]);
			oldGroupNum = newGroupNum;
		}
	}
	return 0;
}
