# Flag Indicator

Minimal program to show a country flag for the current keyboard layout on X11

# Requires

Vala, X11, Make, Gtk, libappindicator-gtk3

# Building

* `git clone --recursive https://codeberg.org/nazrin/flagindicator`
* `cd flagindicator`
* `make`

